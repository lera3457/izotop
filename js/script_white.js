$(document).ready(function(){

    $('.slider_header').slick
    ({
        prevArrow:'<div class="slick-prev slick_button"><img src="./images/prev_link.png"></div>',
        nextArrow:'<div class="slick-next slick_button"><img src="./images/next_link.png"></div>',
        fade: true
    });
    $('.slider_reviews').slick
    ({
        prevArrow:'<div class="slick-prev slick_button"><img src="./images/prev_link.png"></div>',
        nextArrow:'<div class="slick-next slick_button"><img src="./images/next_link.png"></div>',
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1188,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 888,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.menu__icon').on('click', function() {
        $(this).closest('.menu').toggleClass('menu_state_open');
        $('.menu__links').toggleClass('menu_state_open');
    });
    $('input').focus(function(){
        $(this).parents('.form-group').addClass('focused');
    });

    $('input').blur(function(){
        var inputValue = $(this).val();
        if ( inputValue == "" ) {
            $(this).parents('.form-group').removeClass('focused');
        }
    });

});