$(document).ready(function(){

    $('.slider_header').slick
    ({
        prevArrow:'<button type="button" class="slick-prev"></button>',
        nextArrow:'<button type="button" class="slick-next"></button>',
    });
    $('.slider_brand').slick
    ({
        prevArrow:'<button type="button" class="slick-prev"></button>',
        nextArrow:'<button type="button" class="slick-next"></button>',
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 580,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 430,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.menu__icon').on('click', function() {
        $(this).closest('.menu').toggleClass('menu_state_open');
        $('.menu__links').toggleClass('menu_state_open');
    });

    if($('.container').parent().hasClass('contact_header')){
        ymaps.ready(init);

        function init(){
            var myMap = new ymaps.Map("map", {
                center: [46.368497, 48.044844],
                zoom: 17
            });

            var myPlacemark = new ymaps.Placemark([46.368497, 48.044844], {
                hintContent: 'Изотоп',
                balloonContent: 'Изотоп, 414056, ул. М. Максаковой, 16'
            },{
                iconLayout: 'default#image',
                iconImageHref: './images/iconMap.png',
                iconImageSize: [24, 35],
                iconImageOffset: [-15, -30],
                zIndex: 99
            });

            myMap.geoObjects.add(myPlacemark);
        }
    }

});
